package com.k15t.examples;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.function.UnaryOperator;


public class SlideShowRecipe extends Recipe {

    public SlideShowRecipe() {

    }


    protected String getPlaceholderHtml() {
        return ""
                + "<div class=\"nav sp-slideshow\""
                + "    data-viewport-id=\"$viewport.id\""
                + "    loop=-1"
                + "     delay=2000"
                + "     cycleSpeed=2000"
                + "     cycleInterval=2000"
                + "<div>";
    }


    protected List<UnaryOperator<String>> getPostProcessors() {

        return ImmutableList.of(new SlideShowRecipe.SlideShowPostProcessor());
    }


    protected List<String> getWebResources() {
        return ImmutableList.of("com.k15t.hacketse.viewport.recipe.viewport-recipes:recipe-slideShow");
    }


    class SlideShowPostProcessor implements UnaryOperator<String> {


        @Override
        public String apply(String s) {
            return StringUtils.replace(s,
                    "</body>",
                    "<script>"
                            + "$('.sp-slideshow').each(function (index) {\n"
                            + "    $(this).attr(\"data-cycle-pause-on-hover\", \"true\")\n"
                            + "           .attr(\"data-cycle-swipe\", \"true\")\n"
                            + "           .cycle();\n"
                            + "});"
                            + "</script>"
                            + "</body>"
            );

        }
    }

}
