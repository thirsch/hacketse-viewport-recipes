package com.k15t.examples;


import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.function.UnaryOperator;


// Can be used as $recipe.test in viewport theme code.
public class RecipeTest extends Recipe {


    public RecipeTest() {

    }


    protected String getPlaceholderHtml() {
        return "<b>Test</b>";
    }


    protected List<UnaryOperator<String>> getPostProcessors() {
        return ImmutableList.of(new TestPostProcessor());
    }


    class TestPostProcessor implements UnaryOperator<String> {

        @Override
        public String apply(String s) {
            return s.replace("</body>", "<script>console.log('foo')</script></body>");
        }

    }

}
