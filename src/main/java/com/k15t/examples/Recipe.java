package com.k15t.examples;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;


public abstract class Recipe implements Supplier<Map<String, Object>> {

    public Map<String, Object> get() {
        return ImmutableMap.of(
                // HTML that the placeholder will render in its place
                "html", getPlaceholderHtml(),
                // post processors that runs over the final HTML string and can add additional resources like Javascript <script> tags
                "postProcessors", getPostProcessors(),

                "webResources", getWebResources()
        );
    }


    protected String getPlaceholderHtml() {
        return StringUtils.EMPTY;
    }


    protected List<UnaryOperator<String>> getPostProcessors() {
        return ImmutableList.of();
    }


    protected List<String> getWebResources() {
        return ImmutableList.of();
    }

}
