package com.k15t.examples;


import com.google.common.collect.ImmutableList;

import java.util.List;


public class CommentsRecipe extends Recipe {


    protected String getPlaceholderHtml() {
        return ""
                + "<h2>Comments</h2>"
                + "#macro(printCommentThread $comment)"
                + "<li class=\"comment\">"
                + "  <div class=\"comment-header\">"
                + "    $comment.author"
                + "    (created: $comment.creationDate(\"yyyy-MM-dd hh:mm\"),"
                + "    last modified: $comment.lastModificationDate(\"yyyy-MM-dd hh:mm\"))"
                + "  </div>"
                + "  <div class=\"comment-content\">"
                + "    <p class=\"comment-body\">$comment.content</p>"
                + "  </div>"
                + "  <div><a class=\"comment-reply-link\" data-comment-id=\"$comment.id\" href=\"#\">Reply</a></div>"
                + "  #foreach($child in $comment.children)"
                + "    <ol class=\"comment-threads\">"
                + "   #printCommentThread($child)"
                + "    </ol>"
                + "  #end"
                + "</li>"
                + "#end"
                + "<ol id=\"comments-section\" class=\"comment-threads top-level\">"
                + "  #foreach($comment in $context.page.topLevelComments)"
                + "    #printCommentThread($comment)"
                + "  #end"
                + "</ol>"
                + "<script type=\"text/javascript\">"
                + "    var pageId = '$context.page.id';"
                + "    var commentResourcePath = '$context.comments.restUrl';"
                + "</script>";
    }


    protected List<String> getWebResources() {
        return ImmutableList.of("com.k15t.hacketse.viewport.recipe.viewport-recipes:comments");
    }

}
