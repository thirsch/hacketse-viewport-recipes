package com.k15t.examples;


import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.function.UnaryOperator;


public class PageTreeRecipe extends Recipe {


    public PageTreeRecipe() {
    }


    protected String getPlaceholderHtml() {
        return ""
                + "<ul class=\"nav sp-tree\""
                + "    data-viewport-id=\"$context.viewport.id\""
                + "    data-root=\"$context.tree.root.link\""
                + "    data-current=\"$context.page.link\">"
                + "</ul>";
    }


    protected List<UnaryOperator<String>> getPostProcessors() {

        return ImmutableList.of(new PageTreePostProcessor());
    }

    protected List<String> getWebResources() {
        return ImmutableList.of("com.k15t.hacketse.viewport.recipe.viewport-recipes:scroll-tree");
    }


    class PageTreePostProcessor implements UnaryOperator<String> {

        @Override
        public String apply(String s) {
            return StringUtils.replace(
                    s,
                    "</body>",
                    ""
                            + "<script>"
                            + "$(document).ready(function () {"
                            + "    $('.sp-tree').scrollTree({"
                            + "        'contextPath': AJS.contextPath()"
                            + "    });"
                            + "});"
                            + "</script>"
                            + "</body>"
            );
        }

    }

}
