$(document).ready(function () {

    var generateFormId = function(commentId) {
        var formId = "comment-submit-form";
        return commentId ? formId + "-" + commentId : formId;
    };

    var generateCommentReplyForm = function(formId, commentId) {
        return '<form id="' + formId + '">' +
            '  <input type="text" name="pageId" value="' + pageId + '" hidden="true">' +
            '  <input type="text" name="replyTo" value="' + commentId + '" hidden="true">' +
            '  <textarea name="content" class="recipe-comments-area" rows="5" cols="30"></textarea><br>' +
            '  <select name="contentType">' +
            '    <option value="markdown">Markdown</option>' +
            '    <option value="plain">Plain</option>' +
            '  </select>' +
            '  <input type="submit" value="Submit">' +
            '</form>';
    };

    var submitFormEvent = function(event) {
        var data = {
            'pageId': this.elements.pageId.value,
            'replyTo': this.elements.replyTo.value,
            'content': this.elements.content.value,
            'contentType': this.elements.contentType.value,
        };
        $.ajax({
            type: "POST",
            url: commentResourcePath,
            data: $.param(data),
            success: function() {
                location.reload();
            },
            error: function() {
                alert("There was an error submitting the comment.");
            }
        });

        event.preventDefault();
    };

    $('#comments-section').after(generateCommentReplyForm('comment-submit-form', ''));
    $('#comment-submit-form').submit(submitFormEvent);

    $('.comment-reply-link').click(function(event) {
        var commentId = this.dataset.commentId;
        var formId = generateFormId(commentId);
        var form = $('#' + formId);
        if (form.length) {
            form.remove();
        } else {
            $(this).after(generateCommentReplyForm(formId, commentId));
            $('#' + formId).submit(submitFormEvent);
        }
        event.preventDefault();
    });

});